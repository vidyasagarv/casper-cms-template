+++
title = "Theory of groups for physics applications"
author = ["Vidya Sagar V"]
description = "Notes for the [[NPTEL lecture][https://onlinecourses.nptel.ac.in/noc18_ph11/course]] titled the same."
date = 2018-08-13T00:00:00+05:30
draft = false
+++

## Week 1 {#week-1}


### Introduction {#introduction}

-   The origins of Group theory is in Premutations (the algebra obeyed) and Geometry (rotations) corresponding to Discrete and Continuous groups respectively.
-   Geometric rotations, in general do not commute.
-   Continuous groups is essentially Trignomentry.
-   In quantum mechanics, symmetry group substitutes for the geometry of shape and size.


### Algebraic preliminaries {#algebraic-preliminaries}


#### Sets and maps {#sets-and-maps}

Mathematicians usually classify maps as:

-   Surjective/onto: Range is completely covered.
-   Injective/into: One to One, but need not exhaust range.
-   Bijective: Both one-to-one and covers the whole range.

Algebraic Structure: There exists a binary relation with properties:

-   commutatitive: \\(a \circ b\\) = \\(b \circ a\\)
-   associative: \\(a \circ (b \circ c) = (a \circ b) \circ c\\)
    -   When not associative, Jacobi identity is used as an alternative.
-   identity element: \\(a \circ e = a = e \circ a\\)
-   inverse: \\(a \circ a^{-1} = e = a^{-1} \circ a\\)

**Homomorphism**: Maps which preserves algebraic structure.

**Isomorphism**: Maps which are bijective and preserves the algrbraic structure i.e., a map M: \\(S\_1 \rightarrow S\_2\\) with algrbraic structures \\(a \circ\_1 b \in S\_1\\) and \\(a' \circ\_2 b' \in S\_2\\) has the property - \\((a \circ\_1 b) \rightarrow (a' \circ\_2 b')\\) if \\(a \rightarrow a'\\) and \\(b \rightarrow b'\\).


#### Groups {#groups}

**Group**: A set which satisfies:

1.  Closure: \\(\forall a, b \in S\ \exists\ a \circ b \in S\\)
2.  Associative: \\(a \circ (b \circ c) = (a \circ b) \circ c\\)
3.  Identity: \\(\exists\ e \in S\\) such that \\(a \circ e = a = e \circ a\\)
4.  Inverse: \\(\forall a \in S\ \exists\ a^{-1} \in S\\) such that \\(a \circ a^{-1} = e = a^{-1} \circ a\\)

Examples of groups: set of matrices with determinant \\(\ne\\) 0; set of all posible rotational configurations of a rigid object.

**Abelian group**: A group which satisfies an additional property - Commutativiy: \\(a \circ b\\) = \\(b \circ a\\). Example: Rotations in 2D plane - SO(2).


#### Linear vector space {#linear-vector-space}

A set \\(V\\) with + (addition) and \\(\bullet\\) (scalar multiplication) and an auxiliary set of scalars (s) which satisfies:

-   Abelian group under +
-   Under \\(\bullet\\):
    -   \\(a \bullet v \in V\\) when \\(a \in s\\)
    -   \\(a \bullet (v\_1 + v\_2) = a \bullet v\_1 + a \bullet v\_2\\): multiplication is distributive
    -   Additionally, scalars have their own abelian structure with + so that \\(a\_1+a\_2 \in s\\) etc.
    -   \\((a\_1+a\_2) \bullet v = a\_1 \bullet v + a\_2 \bullet v\\)


#### Permutations {#permutations}

Permutation group is the group of all possible ways of rearranging n objects. The "possible ways" are elements of the group.

Any discrete group is a sub-group of some permutation group.

Can be represented as matrices.


#### Equivalence realtion {#equivalence-realtion}

For a set \\(s\\) a "relation" \\(R\\) is a _conditional_ about a,b etc. \\(\in s\\) such that:

-   \\(a R a\\) : a is always related to a: Reflexivity
-   \\(a R b \Rightarrow b R a\\): Symmetry
-   \\(a R b\\) and \\(b R c \Rightarrow a R c\\): Tansitivity

Any relation \\(R\\) with above properties is called an _Equivalence relation_. Example: "parallel" relation of st. lines in a plane is an equivalence relation ; "perpendicular" is not an equivalence relation (does not satisfy first req.)

**Theorem**: An equivalence relation divides a set into disjoint subsets whose union makes up the whole set \\(s\\).

**Proof**: Let $s\_1, s\_2, ...$ be some subsets.
Let \\(s\_1\\) be such that all elements in it are related by R. Similarly, consider \\(s\_2\\). Now, \\(s\_1 \cap s\_2\\) because \\(a \in s\_1\\) and \\(b \in s\_2\\) and hypothesis \\(a R b\\) forces \\(s\_1\\) and \\(s\_2\\) to be same subsets due to transitivity and symmetry.


## Week 2 {#week-2}


### Lagrange's Theorem: {#lagrange-s-theorem}

If H is a subgroup


## Reference books: {#reference-books}

-   Morton Hammermesh
-   Sadri Hassani Ch. 23 and 24
-   Brian C. Hall
-   Ramadevi's draft book for applications
