+++
title = "English grammar"
author = ["Vidya Sagar V"]
description = "Personal notes of the book 'English grammar in use' - Raymond Murphy."
date = 2018-08-13T00:00:00+05:30
draft = false
+++

## Present continuous (I am doing) {#present-continuous--i-am-doing}

**Usage**: in the middle of 'action', started doing and haven't finished yet; talk about things/changes happening in a period around now (for example, today / this week / this evening etc.).

Often the action is happening at the time of speaking, but not necessarily.

**Standard structure**:

| Subject                    | Present tense of _be_                | Present participle verb |
|----------------------------|--------------------------------------|-------------------------|
| I                          | am ('m) / am not ('m not, ain't)     | -ing form of verb       |
| he/she/it/name of a person | is ('s) / is not ('s not, isn't)     | -ing form of verb       |
| we/you/they/name of group  | are ('re) / are not ('re not,aren't) | -ing form of verb       |

For framing a question, reverse the first 2 columns.

Continuous is not used with stative verbs, use simple instead.


## Present simle (I do) {#present-simle--i-do}
